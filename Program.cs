﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace letztesÄnderungsdatum
{
    class Program
    {
        static string newestFile;
        List<string> list = new List<string>();

        [STAThread]
        static void Main(string[] args)
        {
            if ( args.Length == 0 )
            {
                Console.WriteLine("Enter a path. Press any key to continue...");
                Console.ReadKey();
                Environment.Exit(0);
            }

            if ( !Directory.Exists(args[0]) ) 
            {
                Console.WriteLine("Path doesn't exist. Press any key to continue...");
                Console.ReadKey();
                Environment.Exit(0);
            }

            modifyDate mod = new modifyDate();
            mod.getLastModifiedDate(args[0]);

            Console.WriteLine(newestFile);

            Console.WriteLine("Weitersuchen? (y/n)");
            ConsoleKeyInfo keyPressed = Console.ReadKey();

            while (keyPressed.Key.ToString() == "Y")
            {
                mod.resetNewestFile();
                mod.getLastModifiedDate(args[0]);

                Console.WriteLine();
                Console.WriteLine(newestFile);

                Console.WriteLine("Weitersuchen? (y/n)");
                keyPressed = Console.ReadKey();
            }
        }

        public void setNewestFile(string file)
        {
            newestFile = file;
        }

        public string getNewestFile()
        {
            return newestFile;
        }

        public void addToList(string file)
        {
            list.Add(file);
        }

        public List<string> getList()
        {
            return list;
        }
    }

    public class modifyDate
    {
        Program prog = new Program();

        public void resetNewestFile()
        {
            prog.setNewestFile(null);
        }

        public void getLastModifiedDate(String path)
        {
            
            String[] dir = Directory.GetFileSystemEntries(path);
            List<string> list = prog.getList();
            int fileAlreadyRead = 0;

            for (int i = 0; i < dir.Length; i++)
            {
                FileAttributes attr = File.GetAttributes(dir[i]);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    getLastModifiedDate(dir[i]);
                }
                else
                {
                    for (int p = 0; p < list.Count; p++)
                    {
                        if (dir[i] == list[p])
                        {
                            fileAlreadyRead = 1;
                        }
                    }
                    if (fileAlreadyRead == 1)
                    {
                        fileAlreadyRead = 0;
                        if (i == dir.Length - 1)
                        {
                            prog.addToList(prog.getNewestFile());
                        }
                        continue;
                    }
                    DateTime modifyDateCurrentFile = File.GetLastWriteTime(dir[i]);


                    if (prog.getNewestFile() == null)
                    {
                        prog.setNewestFile(dir[i]);
                        continue;
                    }
                    DateTime modifyDateNewestFile = File.GetLastWriteTime(prog.getNewestFile());

                    if ( DateTime.Compare(modifyDateCurrentFile, modifyDateNewestFile) >= 0 )  
                    {
                        prog.setNewestFile(dir[i]);
                    }
                }

                if (i == dir.Length - 1)
                {
                    prog.addToList(prog.getNewestFile());
                }
            }
        }
    }
}
